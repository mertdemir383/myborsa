package com.borsa;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

@ManagedBean (name = "myExcelFile")
@SessionScoped
public class ExcelFile {
	public void getExcelFile() throws IOException
	{
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("List Currency");
		Row rowHeading = sheet.createRow(0);
		
		ParamArray paramArray = new ParamArray();
 		ArrayList<String> param = paramArray.getParamArray();
		
		for(int i = 0 ; i < param.size(); i++)
		rowHeading.createCell(i).setCellValue(param.get(i));
		
		for(int i = 0 ; i < param.size(); i++)
		{
			CellStyle styleRowHeading = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName(HSSFFont.FONT_ARIAL);
			font.setFontHeightInPoints((short)11);
			font.setColor(HSSFColor.RED.index);
			styleRowHeading.setFont(font);
			styleRowHeading.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			rowHeading.getCell(i).setCellStyle(styleRowHeading);
		}
		
		int r = 1;
		
		for(ExchangeRateItem exRateItem : ComboboxValues.currencyListStatic)
		{
			Row row  = sheet.createRow(r);
			
			Cell cellTarih = row.createCell(0);
			cellTarih.setCellValue(exRateItem.getTarih());
			
			Cell cellCurrencyCode = row.createCell(1);
			cellCurrencyCode.setCellValue(exRateItem.getCurrencyCode());
			
			Cell cellUnit = row.createCell(2);
			cellUnit.setCellValue(exRateItem.getUnit());
			
			Cell cellIsim = row.createCell(3);
			cellIsim.setCellValue(exRateItem.getIsim());
			
			Cell cellCurrencyName = row.createCell(4);
			cellCurrencyName.setCellValue(exRateItem.getCurrencyName());
			
			Cell cellForexBuying = row.createCell(5);
			cellForexBuying.setCellValue(exRateItem.getForexBuying());
			
			Cell cellForexSelling = row.createCell(6);
			cellForexSelling.setCellValue(exRateItem.getForexSelling());
			
			Cell cellBanknoteBuying = row.createCell(7);
			cellBanknoteBuying.setCellValue(exRateItem.getBanknoteBuying());
			
			Cell cellBanknoteSelling = row.createCell(8);
			cellBanknoteSelling.setCellValue(exRateItem.getBanknoteSelling());
			
			Cell cellCrossRateUSD = row.createCell(9);
			cellCrossRateUSD.setCellValue(exRateItem.getCrossRateUSD());
			
			Cell cellCrossRateOther = row.createCell(10);
			cellCrossRateOther.setCellValue(exRateItem.getCrossRateOther());
			
			
			r++;
		}
		
		for(int i = 0 ; i < param.size() ; i++)
		{
			sheet.autoSizeColumn(i);
		}
		
		FileOutputStream fos = new FileOutputStream("C:/Users/SOLVIA/Desktop/currencyList.xls");
		workbook.write(fos);
		fos.close();
	}
}
