package com.borsa;

import java.io.Serializable;
import java.util.ArrayList;

public class CurrencyValue implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> tarih;
	private ArrayList<String> currencyCode;
	private ArrayList<String> unit;
	private ArrayList<String> isim;
	private ArrayList<String> currencyName;
	private ArrayList<String> forexBuying;
	private ArrayList<String> forexSelling;
	private ArrayList<String> banknoteBuying;
	private ArrayList<String> banknoteSelling;
	private ArrayList<String> crossRateUSD;
	private ArrayList<String> crossRateOther;
	
	public ArrayList<String> getTarih() {
		return tarih;
	}
	public void setTarih(ArrayList<String> tarih) {
		this.tarih = tarih;
	}
	public ArrayList<String> getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(ArrayList<String> currencyCode) {
		this.currencyCode = currencyCode;
	}
	public ArrayList<String> getUnit() {
		return unit;
	}
	public void setUnit(ArrayList<String> unit) {
		this.unit = unit;
	}
	public ArrayList<String> getIsim() {
		return isim;
	}
	public void setIsim(ArrayList<String> isim) {
		this.isim = isim;
	}
	public ArrayList<String> getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(ArrayList<String> currencyName) {
		this.currencyName = currencyName;
	}
	public ArrayList<String> getForexBuying() {
		return forexBuying;
	}
	public void setForexBuying(ArrayList<String> forexBuying) {
		this.forexBuying = forexBuying;
	}
	public ArrayList<String> getForexSelling() {
		return forexSelling;
	}
	public void setForexSelling(ArrayList<String> forexSelling) {
		this.forexSelling = forexSelling;
	}
	public ArrayList<String> getBanknoteBuying() {
		return banknoteBuying;
	}
	public void setBanknoteBuying(ArrayList<String> banknoteBuying) {
		this.banknoteBuying = banknoteBuying;
	}
	public ArrayList<String> getBanknoteSelling() {
		return banknoteSelling;
	}
	public void setBanknoteSelling(ArrayList<String> banknoteSelling) {
		this.banknoteSelling = banknoteSelling;
	}
	public ArrayList<String> getCrossRateUSD() {
		return crossRateUSD;
	}
	public void setCrossRateUSD(ArrayList<String> crossRateUSD) {
		this.crossRateUSD = crossRateUSD;
	}
	public ArrayList<String> getCrossRateOther() {
		return crossRateOther;
	}
	public void setCrossRateOther(ArrayList<String> crossRateOther) {
		this.crossRateOther = crossRateOther;
	}
	
	
}
