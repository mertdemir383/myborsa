package com.borsa;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean(name = "xGetSetVariable")
@SessionScoped
public class GetSetVariable implements Serializable {

	private static final long serialVersionUID = 1L;
	public static ArrayList<String> currencyWaers;
	public static ArrayList<String> currencyLtext;
	public static ArrayList<String> parameterList;
	

	public static ArrayList<String> getCurrencyWaers() {
		return currencyWaers;
	}
	public static void setCurrencyWaers(ArrayList<String> currencyWaers) {
		GetSetVariable.currencyWaers = currencyWaers;
	}
	public static ArrayList<String> getCurrencyLtext() {
		return currencyLtext;
	}
	public static void setCurrencyLtext(ArrayList<String> currencyLtext) {
		GetSetVariable.currencyLtext = currencyLtext;
	}
	public static ArrayList<String> getParameterList() {
		return parameterList;
	}
	public static void setParameterList(ArrayList<String> parameterList) {
		GetSetVariable.parameterList = parameterList;
	}
	
	
}
