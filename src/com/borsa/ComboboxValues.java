package com.borsa;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

@ManagedBean(name = "xxxMertComboboxValues")
@SessionScoped
public class ComboboxValues implements Serializable {

	private static final long serialVersionUID = 1L;
	public ArrayList<String> combobox; 
	private ArrayList<String> value;
	private List<ExchangeRateItem> currencyList = null;
	public static List<ExchangeRateItem> currencyListStatic ;
	

	public List<ExchangeRateItem> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<ExchangeRateItem> currencyList) {
		this.currencyList = currencyList;
	}
	
	public ArrayList<String> getValue() {
		return value;
	}

	public void setValue(ArrayList<String> value) {
		this.value = value;
	}

	public ArrayList<String> getCombobox() {
		return combobox;
	}

	public void setCombobox(ArrayList<String> combobox) {
		this.combobox = combobox;
	}
	
	public LinkedHashMap<String,String> getAllCurrencyKod() {
		
		CurrencyWSDL cWSDL = new CurrencyWSDL();
		ArrayList<String> LtextList;
		ArrayList<String> WaersList;
		
		LinkedHashMap<String, String> currencyKod = new LinkedHashMap<String, String>();
		try {
			LtextList = cWSDL.getCurrencyLtext();
			WaersList = cWSDL.getCurrencyWaers();
			for (int counter = 0; counter < LtextList.size(); counter++) 
				{
					String getDataOnebyOneLtext = LtextList.get(counter);
					String getDataOnebyOneWears = WaersList.get(counter);
					currencyKod.put(getDataOnebyOneLtext, getDataOnebyOneWears);
				}
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return currencyKod;
		}
	
	public ArrayList<String> getComboboxValue() throws ParserConfigurationException, ParseException, SAXException, IOException
	{
		ArrayList<String> strDate = DateView.doParseIndexDate2;
		CurrencyWSDL cWSDL = new CurrencyWSDL();
		DateView dv = new DateView();
		
		ArrayList<String> CurrencyWearsList = cWSDL.getCurrencyWaers();
		
		ArrayList<String> CurrencyValueList = dv.doParseIndexDate();
		
		String strList = CurrencyValueList.toString();
		String str = strList.substring(1, strList.length()-1);
		
		String strListArray[] = str.split(Pattern.quote(","));
		
		ArrayList<String> valueList = new ArrayList<String>();
		
			for(int i = 0; i < CurrencyWearsList.size(); i++)
			{
				for(int k = 0; k < strDate.size(); k ++)
				{
					if(combobox.contains(CurrencyWearsList.get(i)))
						valueList.add(strListArray[ i + k + (k*CurrencyWearsList.size()) ]);
				}
			}
			
			value = valueList;		
			 
			 	getSetValue(valueList);
			 	
			return value;
				
	} 

	public void getSetValue(ArrayList<String> valueList) throws IOException
	{ 
		
		ParamArray paramArray = new ParamArray();  
		ArrayList<String> paramArrayList = paramArray.getParamArray();
		
		String paramListStr = valueList.toString();
		String paramListStr1= paramListStr.substring(1, paramListStr.length()-1).replaceAll(Pattern.quote(","), "") + " ";//size 1 eksik al...
		String paramList[] = paramListStr1.split(Pattern.quote("*"));
		
		
		ArrayList<String> tarihList = new ArrayList<String>();
		ArrayList<String> currencyCodeList = new ArrayList<String>();
		ArrayList<String> unitList = new ArrayList<String>();
		ArrayList<String> isimList = new ArrayList<String>();
		ArrayList<String> currencyNameList = new ArrayList<String>();
		ArrayList<String> forexBuyingList = new ArrayList<String>();
		ArrayList<String> forexSellingList = new ArrayList<String>();
		ArrayList<String> banknoteBuyingList = new ArrayList<String>();
		ArrayList<String> banknoteSellingList = new ArrayList<String>();
		ArrayList<String> crossRateUSDList = new ArrayList<String>();
		ArrayList<String> crossRateOtherList = new ArrayList<String>();
		
		for(int i = 0 ; i < valueList.size() ; i++)
		{
			for(int k = 0; k < paramArrayList.size(); k = paramArrayList.size()+1)
			{
				tarihList.add(paramList[k + i*paramArrayList.size()]);
				currencyCodeList.add(paramList[k+1 + i*paramArrayList.size()]);
				unitList.add(paramList[k+2 + i*paramArrayList.size()]);
				isimList.add(paramList[k+3 + i*paramArrayList.size()]);
				currencyNameList.add(paramList[k+4 + i*paramArrayList.size()]);
				forexBuyingList.add(paramList[k+5 + i*paramArrayList.size()]);
				forexSellingList.add(paramList[k+6 + i*paramArrayList.size()]);
				banknoteBuyingList.add(paramList[k+7 + i*paramArrayList.size()]);
				banknoteSellingList.add(paramList[k+8 + i*paramArrayList.size()]);
				crossRateUSDList.add(paramList[k+9 + i*paramArrayList.size()]);
				crossRateOtherList.add(paramList[k+10 + i*paramArrayList.size()]);
			}
		}
		
		CurrencyValue cValue = new CurrencyValue();
		cValue.setTarih(tarihList);
		cValue.setCurrencyCode(currencyCodeList);
		cValue.setUnit(unitList);
		cValue.setIsim(isimList);
		cValue.setCurrencyName(currencyNameList);
		cValue.setForexBuying(forexBuyingList);
		cValue.setForexSelling(forexSellingList);
		cValue.setBanknoteBuying(banknoteBuyingList);
		cValue.setBanknoteSelling(banknoteSellingList);
		cValue.setCrossRateUSD(crossRateUSDList);
		cValue.setCrossRateOther(crossRateOtherList);
		
		getValueList(tarihList, cValue);
		
	}

	private void getValueList(ArrayList<String> tarihList, CurrencyValue cValue) {
		currencyList = new ArrayList<ExchangeRateItem>();
		for(int i = 0 ; i < tarihList.size(); i++)
		{
			currencyList.add(new ExchangeRateItem(cValue.getTarih().get(i),cValue.getCurrencyCode().get(i), cValue.getUnit().get(i),
					cValue.getIsim().get(i),cValue.getCurrencyName().get(i), cValue.getForexBuying().get(i), cValue.getForexSelling().get(i),
					cValue.getBanknoteBuying().get(i), cValue.getBanknoteSelling().get(i), cValue.getCrossRateUSD().get(i), cValue.getCrossRateOther().get(i)));
		}
		
		currencyListStatic  = currencyList;
	}

}


