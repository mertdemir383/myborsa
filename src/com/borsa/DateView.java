package com.borsa;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@ManagedBean(name = "xdateView")
@SessionScoped

public class DateView {
	
	public Date date1 = new Date();
	public Date date2 = new Date(); 
	private String outputCalendarDateText;
	public static ArrayList<String> doParseIndexDate2 = new ArrayList<String>();

	
	public Date getDate1() {
		return date1;
	}
	public void setDate1(Date date1) {
		this.date1 = date1;
	}
	public Date getDate2() {
		return date2;
	}
	public void setDate2(Date date2) {
		this.date2 = date2;
	}
	public String getOutputCalendarDateText() {
		return outputCalendarDateText;
	}
	public void setOutputCalendarDateText(String outputCalendarDateText) {
		this.outputCalendarDateText = outputCalendarDateText;
	}

	public ArrayList<String> getParseTestDate() throws ParserConfigurationException, SAXException, IOException
	{
		ArrayList<String> dateList = new ArrayList<String>();
		
		File xmlFile = new File("C:/Users/SOLVIA/Desktop/BorsaWS/Borsa/WebContent/Date.xml");
									
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("item");
		
		for(int temp = 0 ; temp < nList.getLength() ; temp++)
		{
			Node nNode = nList.item(temp);
			if(nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element)nNode;
				String str = eElement.getElementsByTagName("Periodat").item(0).getTextContent();
				dateList.add(str);
			}
		}
		return dateList;
	}
	
	public String getCalendarDate1() throws ParseException, ParserConfigurationException, SAXException, IOException
	{
		String temp = date1.toString();
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		date1 = (Date)formatter.parse(temp);
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(date1);
		
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int mon = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		
		String outputMon, outputDay;
		
		if(mon != 10 && mon != 11 && mon != 12)
			outputMon = "0" + Integer.toString(mon);
		else
			outputMon = Integer.toString(mon);
		
		if(day == 1 || day == 2 || day == 3 || day == 4 || day == 5 || day == 6 || day == 7 || day == 8 || day == 9)
			outputDay = "0" + Integer.toString(day);
		else
			outputDay = Integer.toString(day);
		
		String outputCalendarDate = year + "-" + outputMon + "-" + outputDay;
			
		return outputCalendarDate;
	}
	
	public String getCalendarDate2() throws ParseException, ParserConfigurationException, SAXException, IOException
	{
		String temp = date2.toString();
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		date2 = (Date)formatter.parse(temp);
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(date2);
		
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int mon = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		
		String outputMon, outputDay;
		
		if(mon != 10 && mon != 11 && mon != 12)
			outputMon = "0" + Integer.toString(mon);
		else
			outputMon = Integer.toString(mon);
		
		if(day == 1 || day == 2 || day == 3 || day == 4 || day == 5 || day == 6 || day == 7 || day == 8 || day == 9)
			outputDay = "0" + Integer.toString(day);
		else
			outputDay = Integer.toString(day);
		
		String outputCalendarDate = year + "-" + outputMon + "-" + outputDay;
			
		return outputCalendarDate;
	}
	
	public String parseTestDateContainsCalDate() throws ParserConfigurationException, SAXException, IOException, ParseException
	{
		ArrayList<String> parseTestDate = getParseTestDate();
		String calDate1 = getCalendarDate1();
		String calDate2 = getCalendarDate2();
		
		ArrayList<String> indexDate = new ArrayList<String>();
		
		if(parseTestDate.contains(calDate1) && parseTestDate.contains(calDate2))
		{
			int indexDate1 = parseTestDate.indexOf(calDate1);
			int indexDate2 = parseTestDate.indexOf(calDate2);
			
			for(int i = indexDate1 ; i < indexDate2 ; i++)
				 indexDate.add(parseTestDate.get(i));
			
			outputCalendarDateText = "";
		}
		else
			outputCalendarDateText = "gecerli tarih giriniz";
		
		doParseIndexDate2 = indexDate;
		
		return outputCalendarDateText;
	}
	
	public ArrayList<String> doParseIndexDate() throws ParserConfigurationException, ParseException, SAXException, IOException
	{
		ArrayList<String> myList = new ArrayList<String>();
		
		ArrayList<String> doParseIndexDateList = new ArrayList<String>();
		doParseIndexDateList = doParseIndexDate2;
	
		for(int i = 0 ; i < doParseIndexDateList.size() ; i++)
		{
			String str = null;
			str = doParseIndexDateList.get(i);
			
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = (Date)formatter.parse(str);
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(date);
			
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int mon = cal.get(Calendar.MONTH) + 1;
			int year = cal.get(Calendar.YEAR);
			
			String outputMon, outputDay;
			
			if(mon != 10 && mon != 11 && mon != 12)
				outputMon = "0" + Integer.toString(mon);
			else
				outputMon = Integer.toString(mon);
			
			if(day == 1 || day == 2 || day == 3 || day == 4 || day == 5 || day == 6 || day == 7 || day == 8 || day == 9)
				outputDay = "0" + Integer.toString(day);
			else
				outputDay = Integer.toString(day);
				

				String outputDate = Integer.toString(year) + outputMon + "/" + outputDay + outputMon+ Integer.toString(year);
				
				String url = "http://www.tcmb.gov.tr/kurlar/" + outputDate + ".xml";
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(url);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("Currency");
				for (int temp1 = 0; temp1 < nList.getLength(); temp1++) {
					Node nNode = nList.item(temp1);

					if (nNode.getNodeType() == Node.ELEMENT_NODE)
					{
						Element eElement = (Element) nNode;
						
						ExchangeRateItem er = new ExchangeRateItem();
						
						String tarih = str;
						er.setCurrencyCode(eElement.getAttribute("CurrencyCode"));
						er.setUnit(eElement.getElementsByTagName("Unit").item(0).getTextContent());
						er.setIsim(eElement.getElementsByTagName("Isim").item(0).getTextContent());
						er.setCurrencyName(eElement.getElementsByTagName("CurrencyName").item(0).getTextContent());
						er.setForexBuying(eElement.getElementsByTagName("ForexBuying").item(0).getTextContent());
						er.setForexSelling(eElement.getElementsByTagName("ForexSelling").item(0).getTextContent());
						er.setBanknoteBuying(eElement.getElementsByTagName("BanknoteBuying").item(0).getTextContent());
						er.setBanknoteSelling(eElement.getElementsByTagName("BanknoteSelling").item(0).getTextContent());
						er.setCrossRateUSD(eElement.getElementsByTagName("CrossRateUSD").item(0).getTextContent());
						er.setCrossRateOther(eElement.getElementsByTagName("CrossRateOther").item(0).getTextContent());
						
						getList(myList, tarih, er);
					}
				}
		}
		return myList;
	}

	private void getList(ArrayList<String> myList, String tarih, ExchangeRateItem er) {
		String str1 = tarih + "*" + er.getCurrencyCode() + "*" + er.getUnit() + "*" + er.getIsim() + "*" + er.getCurrencyName() + "*" + er.getForexBuying() + "*" + 
		er.getForexSelling() + "*" + er.getBanknoteBuying() + "*" + er.getBanknoteSelling() + "*" + er.getCrossRateUSD() + "*" + er.getCrossRateOther() + "*";
					
		myList.add(str1);
	}
	
}
