package com.borsa;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean (name = "MyPdfFile")
@SessionScoped
public class PdfFile {
	public void getPdfFile() throws FileNotFoundException, DocumentException
	{	
		ParamArray paramArray = new ParamArray();
		ArrayList<String> paramArrayList = paramArray.getParamArray();
		
		Document doc = new Document(PageSize.A4);
		PdfWriter.getInstance(doc, new FileOutputStream("C:/Users/SOLVIA/Desktop/currencyList.pdf"));
		doc.open();
		
		Font fontHeader = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD ,BaseColor.RED);
		Font fontRow = new Font(Font.FontFamily.TIMES_ROMAN, 6 , Font.NORMAL ,BaseColor.BLUE);
		
		PdfPTable table = new PdfPTable(paramArrayList.size());
		
		for(String header : paramArrayList)
		{
			PdfPCell cell = new PdfPCell();
			cell.getGrayFill();
			cell.setPhrase(new Phrase(header.toUpperCase(),fontHeader));
			table.addCell(cell);
		}
		table.completeRow();
		for(ExchangeRateItem exRateItem : ComboboxValues.currencyListStatic)
		{
			Phrase phraseTarih = new Phrase(exRateItem.getTarih(),fontRow);
			table.addCell(new PdfPCell(phraseTarih));
			
			Phrase phraseCurrencyCode = new Phrase(exRateItem.getCurrencyCode(),fontRow);
			table.addCell(new PdfPCell(phraseCurrencyCode));
			
			Phrase phraseUnit = new Phrase(exRateItem.getUnit(),fontRow);
			table.addCell(new PdfPCell(phraseUnit));
			
			Phrase phraseIsim = new Phrase(exRateItem.getIsim(),fontRow);
			table.addCell(new PdfPCell(phraseIsim));
			
			Phrase phraseCurrencyName = new Phrase(exRateItem.getCurrencyName(),fontRow);
			table.addCell(new PdfPCell(phraseCurrencyName));
			
			Phrase phraseForexBuying = new Phrase(exRateItem.getForexBuying(),fontRow);
			table.addCell(new PdfPCell(phraseForexBuying));
			
			Phrase phraseForexSelling = new Phrase(exRateItem.getForexSelling(),fontRow);
			table.addCell(new PdfPCell(phraseForexSelling));
			
			Phrase phraseBanknoteBuying = new Phrase(exRateItem.getBanknoteBuying(),fontRow);
			table.addCell(new PdfPCell(phraseBanknoteBuying));
			
			Phrase phraseBanknoteSelling = new Phrase(exRateItem.getBanknoteSelling(),fontRow);
			table.addCell(new PdfPCell(phraseBanknoteSelling));
			
			Phrase phraseCrossRateUSD = new Phrase(exRateItem.getCrossRateUSD(),fontRow);
			table.addCell(new PdfPCell(phraseCrossRateUSD));
			
			Phrase phraseCrossRateOther = new Phrase(exRateItem.getCrossRateOther(),fontRow);
			table.addCell(new PdfPCell(phraseCrossRateOther));
		}
		table.completeRow();
		
		doc.add(table);
		doc.close();
	}
}
