package com.borsa;

import java.util.ArrayList;

public class ParamArray {

	public ArrayList<String> getParamArray()
	{
		
		ArrayList<String> myList = new ArrayList<String>();
		myList.add("Tarih");
		myList.add("CurrencyCode");
		myList.add("Unit");
		myList.add("Isim");
		myList.add("CurrencyName");
		myList.add("ForexBuying");
		myList.add("ForexSelling");
		myList.add("BanknoteBuying");
		myList.add("BanknoteSelling");
		myList.add("CrossRateUSD");
		myList.add("CrossRateOther");

		return myList;
		
	}
}
