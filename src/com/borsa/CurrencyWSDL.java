package com.borsa;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class CurrencyWSDL {

	public ArrayList<String> getCurrencyWaers() throws ParserConfigurationException, SAXException, IOException {
		
		ArrayList<String> waersList = new ArrayList<String>();
		File fXmlFile = new File("C:/Users/SOLVIA/Desktop/BorsaWS/Borsa - Copy/WebContent/Currency.xml");//��p
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("item");
		
		for(int temp = 0 ; temp < nList.getLength() ; temp++)
		{
			Node nNode = nList.item(temp);
			if(nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) nNode;
				String WaersJsf = eElement.getElementsByTagName("Waers").item(0).getTextContent();
				waersList.add(WaersJsf);
			}
		}
		
		return waersList;
	}
	public ArrayList<String> getCurrencyLtext() throws ParserConfigurationException, SAXException, IOException {
		
		ArrayList<String> LtextList = new ArrayList<String>();
		File fXmlFile = new File("C:/Users/SOLVIA/Desktop/BorsaWS/Borsa - Copy/WebContent/Currency.xml");
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("item");
		
		for(int temp = 0 ; temp < nList.getLength() ; temp++)
		{
			Node nNode = nList.item(temp);
			if(nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element) nNode;
				String LtextJsf = eElement.getElementsByTagName("Ltext").item(0).getTextContent();
				LtextList.add(LtextJsf);
			}
		}
		return LtextList;
	}
	
	public void getterSetter() throws ParserConfigurationException, SAXException, IOException
	{
		ArrayList<String> waersList = getCurrencyWaers();
		GetSetVariable.setCurrencyWaers(waersList);
		
		ArrayList<String> ltextList = getCurrencyLtext();
		GetSetVariable.setCurrencyLtext(ltextList);
	}
}

