package com.borsa;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "exchangeRateItem")
@SessionScoped
public class ExchangeRateItem  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String tarih;
	private String currencyCode;
	private String unit;
	private String isim;
	private String currencyName;
	private String forexBuying;
	private String forexSelling;
	private String banknoteBuying;
	private String banknoteSelling;
	private String crossRateUSD;
	private String crossRateOther;
	
	public ExchangeRateItem()
	{
		
	}
	
	public ExchangeRateItem(String tarih, String currencyCode, String unit,
			String isim, String currencyName, String forexBuying,
			String forexSelling, String banknoteBuying, String banknoteSelling,
			String crossRateUSD, String crossRateOther) {
		super();
		this.tarih = tarih;
		this.currencyCode = currencyCode;
		this.unit = unit;
		this.isim = isim;
		this.currencyName = currencyName;
		this.forexBuying = forexBuying;
		this.forexSelling = forexSelling;
		this.banknoteBuying = banknoteBuying;
		this.banknoteSelling = banknoteSelling;
		this.crossRateUSD = crossRateUSD;
		this.crossRateOther = crossRateOther;
	}



	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getIsim() {
		return isim;
	}
	public void setIsim(String isim) {
		this.isim = isim;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	public String getForexBuying() {
		return forexBuying;
	}
	public void setForexBuying(String forexBuying) {
		this.forexBuying = forexBuying;
	}
	public String getForexSelling() {
		return forexSelling;
	}
	public void setForexSelling(String forexSelling) {
		this.forexSelling = forexSelling;
	}
	public String getBanknoteBuying() {
		return banknoteBuying;
	}
	public void setBanknoteBuying(String banknoteBuying) {
		this.banknoteBuying = banknoteBuying;
	}
	public String getBanknoteSelling() {
		return banknoteSelling;
	}
	public void setBanknoteSelling(String banknoteSelling) {
		this.banknoteSelling = banknoteSelling;
	}
	public String getCrossRateUSD() {
		return crossRateUSD;
	}
	public void setCrossRateUSD(String crossRateUSD) {
		this.crossRateUSD = crossRateUSD;
	}
	public String getCrossRateOther() {
		return crossRateOther;
	}
	public void setCrossRateOther(String crossRateOther) {
		this.crossRateOther = crossRateOther;
	}
	

	



}
